#!/usr/bin/env bash

for gp in {A..H} ; do
    rm -f "$gp.tmpFile"
done

prefix=$1
files=(`ls | grep "^$prefix"`)

teamCounter=0;
gpCounter=0;


declare -a Teams
declare -A groups

declare -A array_win
declare -A array_draw
declare -A array_lose
declare -A Point
declare -A GoalF
declare -A GoalA
declare -A GoalD

function calcPoint() {
    local _win=$1;
    local _draw=$2;
    local _lose=$3;
    if [[ $_win -eq "" ]]
    then
        _win=0
    fi
    if [[ $_draw -eq "" ]]
    then
        _draw=0
    fi
    if [[ $_lose -eq "" ]]
    then
        _lose=0
    fi

    local _result=$(echo  "scale =0; $_win*3 + $_draw*1 "|bc -l);
    echo $_result
}

function calcGoalDiff() {
    local goalFor=$1;
    local goalAgainst=$2;
    local result=$(echo "scale =0; $goalFor-$goalAgainst"|bc -l);
    echo $result;
}

function justifyWinDrawLose() {
    local leftTeamGoal=$1;
    local rightTeamGoal=$2;
    if [[ ${leftTeamGoal} -eq ${rightTeamGoal} ]]; then
        echo 0
    elif [[ ${leftTeamGoal} -gt ${rightTeamGoal} ]];then
        echo -1
    else echo 1
    fi
}

function initElemToZero()
{
    local returnValue=$1;
    if [[ ($returnValue -eq "") ]]
    then
        eval returnValue=0;
    fi

}
function setArrayWin() {
    local winTeam=$1
    if [[ ! ${array_win[$winTeam]} ]]; then
        array_win[$winTeam]=1
    else
        local tmp=$((${array_win[${winTeam}]}))
        unset array_win[${winTeam}]
        array_win[$winTeam]=$(($tmp+1));
    fi

}
function setArrayDraw() {
    local drawTeam=$1;
    if [[ ! ${array_draw[$drawTeam]} ]]; then
        array_draw[$drawTeam]=1
    else
        local tmp=$((${array_draw[${drawTeam}]}))
        unset array_draw[${drawTeam}}]
        array_draw[$drawTeam]=$(($tmp+1));
    fi
}

function setArrayLose() {
    local loseTeam=$1;
    if [[ ! ${array_lose[$loseTeam]} ]]; then
        array_lose[$loseTeam]=1
    else
        local tmp=$((${array_lose[${loseTeam}]}))
        unset array_lose[${loseTeam}]
        array_lose[$loseTeam]=$(($tmp+1));
    fi
}

function setGoalF() {
    local team=$1;
    local tmpGoal=$2;

    if [[ ! ${GoalF[$team]} ]]; then
        GoalF[${team}]=$tmpGoal;
    else
        local tmp=${GoalF[${team}]};
        unset GoalF[${team}];
        GoalF[${team}]=$(($tmp+${tmpGoal}));
    fi
}

function setGoalA() {
    local team=$1;
    local tmpGoal=$2;
    if [[ ! ${GoalA[$team]} ]]; then
        GoalA[${team}]=$tmpGoal;
    else
        local tmp=${GoalA[${team}]};
        unset GoalA[${team}];
        GoalA[${team}]=$(($tmp+${tmpGoal}));
    fi
}

function setGoalDiff() {
    local team=$1;
    local returnValue=$(calcGoalDiff ${GoalF[$team]} ${GoalA[$team]});

    if [[ ! ${GoalD[$team]} ]]; then
        GoalD[${team}]=$returnValue;
    else
        local tmp=${GoalD[${team}]};
        unset GoalD[${team}];
        GoalD[${team}]=$returnValue;
    fi
}

function prioritize() {
    groupName=$1;
    local zWin=0;
    local zDraw=0;
    local zLose=0;
    local zPoint=0;
    local zGoalF=0;
    local zGoalA=0;
    local zGoalD=0;
    for key in "${!groups[@]}" ; do
        if [[ ${groups[$key]} == $groupName ]];
        then
            printf "%-15s# %-5d# %-5d# %-5d# %-5d# %-5d# %-5d# %-5d \n" $key ${array_win[$key]} ${array_draw[$key]} ${array_lose[$key]} ${Point[$key]} ${GoalF[$key]} ${GoalA[$key]} ${GoalD[$key]} >>"$groupName.tmpFile"
            zWin=$(($zWin+${array_win[$key]}));
            zDraw=$(($zDraw+${array_draw[$key]}))
            zLose=$(($zLose+${array_lose[$key]}))
            zPoint=$(($zPoint+${Point[$key]}))
            zGoalF=$(($zGoalF+${GoalF[$key]}))
            zGoalA=$(($zGoalA+${GoalA[$key]}))
            zGoalD=$(($zGoalD+${GoalD[$key]}))
        fi
    done
    sort -r -n -t# -k8,8 -k6,6 -k1 <"$groupName.tmpFile"
    echo -e "---------------------------------------------------------------"
    printf "%-15s# %-5d# %-5d# %-5d# %-5d# %-5d# %-5d# %-5d \n" "Z" $zWin $zDraw $zLose $zPoint $zGoalF $zGoalA $zGoalD
}

function printTable() {
    for gp in {A..H} ; do
        echo -e "\nGroup $gp"
        printf "%-15s %-5s %-6s %-6s %-6s %-6s %-6s %-6s \n" Team  W D L Point GoalF GoalA GoalD
        prioritize $gp;
    done
}


for item in "${files[@]}"
do
    while read -r teamLine
    do
        returnValue=0
        leftTeam=$(echo ${teamLine} | cut -d " " -f 3)
        rightTeam=$(echo ${teamLine} | cut -d " " -f 6)
        currentGP=$(echo ${teamLine} | cut -d " " -f 2)
        leftTeamGoal=$(echo ${teamLine} | cut -d " " -f 4)
        rightTeamGoal=$(echo ${teamLine} | cut -d " " -f 5)

        groups[$leftTeam]=${currentGP};
        groups[$rightTeam]=${currentGP};

        if [[ ! ${Teams[${leftTeam}]} ]]; then
            Teams[$teamCounter]=${leftTeam}
            teamCounter=$((teamCounter+1))
        fi

        if [[ ! ${Teams[${rightTeam}]} ]]; then
            Teams[$teamCounter]=${rightTeam}
            teamCounter=$((teamCounter+1))
        fi

        returnValue=$(justifyWinDrawLose ${leftTeamGoal} ${rightTeamGoal})

        if [[ ${returnValue} -eq 0 ]]; then
            setArrayDraw ${leftTeam};
            setArrayDraw ${rightTeam};
        elif [[ ${returnValue} -eq -1 ]];then
            setArrayWin ${leftTeam};
            setArrayLose ${rightTeam};
        else
            setArrayWin ${rightTeam};
            setArrayLose ${leftTeam};
        fi

        if [[ ${Point[$leftTeam]}  ]]; then
            if [[ ${array_win[$leftTeam]} -eq "" ]]
            then
                array_win[$leftTeam]=0
            fi
            if [[ ${array_draw[$leftTeam]} -eq "" ]]
            then
               array_draw[$leftTeam]=0
            fi
            if [[ ${array_lose[$leftTeam]} -eq "" ]]
            then
                array_lose[$leftTeam]=0
            fi
            currentPT=${Point[$leftTeam]};
            unset Point[$leftTeam];
            returnValue=$(calcPoint ${array_win[$leftTeam]} ${array_draw[$leftTeam]} ${array_lose[$leftTeam]})
            Point[$leftTeam]=$returnValue
        else
            if [[ ${array_win[$leftTeam]} -eq "" ]]
            then
                array_win[$leftTeam]=0
            fi
            if [[ ${array_draw[$leftTeam]} -eq "" ]]
            then
               array_draw[$leftTeam]=0
            fi
            if [[ ${array_lose[$leftTeam]} -eq "" ]]
            then
                array_lose[$leftTeam]=0
            fi

            returnValue=$(calcPoint ${array_win[$leftTeam]} ${array_draw[$leftTeam]} ${array_lose[$leftTeam]})
            Point[$leftTeam]=${returnValue};
        fi

        if [[ ${Point[$rightTeam]}  ]]; then
            if [[ ${array_win[$rightTeam]} -eq "" ]]
            then
                array_win[$rightTeam]=0
            fi
            if [[ ${array_draw[$rightTeam]} -eq "" ]]
            then
               array_draw[$rightTeam]=0
            fi
            if [[ ${array_lose[$rightTeam]} -eq "" ]]
            then
                array_lose[$rightTeam]=0
            fi

            currentPT=${Point[$rightTeam]};
            unset Point[$rightTeam];
            returnValue=$(calcPoint ${array_win[$rightTeam]} ${array_draw[$rightTeam]} ${array_lose[$rightTeam]})
            Point[$rightTeam]=$returnValue
        else
            if [[ ${array_win[$rightTeam]} -eq "" ]]
            then
                array_win[$rightTeam]=0
            fi
            if [[ ${array_draw[$rightTeam]} -eq "" ]]
            then
               array_draw[$rightTeam]=0
            fi
            if [[ ${array_lose[$rightTeam]} -eq "" ]]
            then
                array_lose[$rightTeam]=0
            fi
            returnValue=$( calcPoint ${array_win[$rightTeam]} ${array_draw[$rightTeam]} ${array_lose[$rightTeam]} )
            Point[$rightTeam]=${returnValue};
        fi

        setGoalF ${leftTeam} ${leftTeamGoal}
        setGoalF ${rightTeam} ${rightTeamGoal}


        setGoalA ${leftTeam} ${rightTeamGoal}
        setGoalA ${rightTeam} ${leftTeamGoal}


        setGoalDiff ${leftTeam}
        setGoalDiff ${rightTeam}
    done <"$item"

done

printTable

for gp in {A..H} ; do
    rm -f "$gp.tmpFile"
done






