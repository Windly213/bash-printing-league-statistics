#ReadMe
####usage`

`./euro.sh score18`

#### order

Reverse order, from small to large

#### Requirement

1. this file requires the permission to read write files, which can generate some files with extension `.tmpFile` temporarily.
2. Tested on Apollo



#### output

![](mdIMG/1.PNG)

![](mdIMG/2.PNG)

![](mdIMG/3.PNG)